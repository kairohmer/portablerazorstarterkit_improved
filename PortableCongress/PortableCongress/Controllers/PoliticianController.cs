﻿using System;
using Congress;
using PortableRazor;
using PortableCongress.Views;
using System.Reflection;
using PortableRazor.Web.Mvc;

namespace PortableCongress
{
	public class PoliticianController : Controller
	{
	    DataAccess _DataAccess;

		public PoliticianController (IHybridWebView webView, DataAccess dataAccess) : base(webView)
		{
			this._DataAccess = dataAccess;
        }

		public void Index()
        {
            // set the model for this view
			ViewBag.Model = _DataAccess.LoadAllPoliticans ();

            // define the view and the layout to show
            View("List");
		}

		public void Details(int id)
        {
            // set the model for this view
            ViewBag.Model = _DataAccess.LoadPolitician(id);

            // here the desired view has the same as calling controller method
            // and we want to use the default layout, so we don't need to pass any arguments
            View();
        }
	}
}

