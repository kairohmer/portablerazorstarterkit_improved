﻿using System;
using Congress;
using PortableRazor;
using PortableCongress.Views;
using System.Reflection;
using PortableRazor.Web.Mvc;

namespace PortableCongress
{
	public class BillController : Controller
	{
	    DataAccess _DataAccess;

		public BillController(IHybridWebView webView, DataAccess dataAccess) : base(webView)
		{
			this._DataAccess = dataAccess;
        }

        // the Index Method is the default View of a controller, but it is optional.
        public void Index()
        {
            // set the model for this view
            ViewBag.Model = _DataAccess.LoadFavoriteBills();

            // show a view with a name different from the method name.
            // you can also force a specific layout to use (this will use the default layout, too).
            View("FavoriteBillList", "_Layout");
        }

        public void Details(int id)
        {
            EvaluateJavascript(
                "$.mobile.loading( 'show', {\n " +
                " text: 'Loading Bill ...',\n " +
                " textVisible: 'false',\n " +
                " theme: 'b',\n " +
                " textonly: 'false' " +
                "});");

            ViewBag.Model = _DataAccess.LoadFavoriteBill(id);
            View("FavoriteBillDetails");
        }


        // overloading view names is supported.
        public async void Details(int id, int politicianid)
        {
            EvaluateJavascript(
                "$.mobile.loading( 'show', {\n " +
                " text: 'Loading Related Bill ...',\n " +
                "textVisible: 'false',\n " +
                " theme: 'b',\n " +
                " textonly: 'false' " +
                "});");

            ViewBag.Model = await WebAccess.GetBillAsync(id);
            ViewBag.Model.PoliticianId = politicianid;

            View("RecentBillDetails");
        }

        public async void ShowRecentBillList(int politicianid)
        {
            // injects java-script code
            EvaluateJavascript(
                "$.mobile.loading( 'show', {\n " +
                " text: 'Loading Recent Votes ...',\n " +
                " textVisible: 'false',\n " +
                " theme: 'b',\n " +
                " textonly: 'false' " +
                "});");

            // download the model data from the web
            ViewBag.Model = await WebAccess.GetRecentVotesAsync(politicianid);
            View("RecentBillList");
        }


        // Methods without views are supported, too.
        // They are used to change the data-model without reloading view.
        // You can call them from the .cshtml by using forms or buttons (todo?)
        public async void AddFavoriteBill(int id)
        {
            var bill = await WebAccess.GetBillAsync (id);
            _DataAccess.SaveFavoriteBill (bill);
        }

        public void RemoveFavoriteBill(int id) {
            _DataAccess.DeleteFavoriteBill (id);

            // call another method to update the view after changing the model
            // .. or really accept an form and go on..
            Index(); 
        }

        public void SaveNotes(int id, string notes) {
            _DataAccess.SaveNotes (id, notes);
        }
	}
}

