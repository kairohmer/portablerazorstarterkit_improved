using System;
using System.IO;
using CoreGraphics;
using System.Reflection;
using Foundation;
using UIKit;
using Congress;
using PortableCongress;

namespace iOSCongress
{
	public partial class CongressViewController : UIViewController
	{
		static bool UserInterfaceIdiomIsPhone {
			get { return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; }
		}

		public CongressViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            // the folder we are working in
            // embedded data will be extracted here and the database for user data is stored here, too
            var applicationDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

            // platform web view to display the content
            // there is probably only one which is passed to every controller
            var hybridWebview = new HybridWebView(webView, Path.Combine(applicationDataFolder, "www"));

            // setup database access
            // this shared between the controllers, but this is up to the application
            var dataAccess = new DataAccess(Path.Combine(applicationDataFolder, "App_Data", DataAccess.DatabaseName));

            // application with two controllers
            var politicianController = new PoliticianController(hybridWebview, dataAccess);
            var billController = new BillController(hybridWebview, dataAccess);

            // setup routing (only one controller here)
            PortableRazor.RouteHandler.RegisterController("Politician", politicianController);
            PortableRazor.RouteHandler.RegisterController("Bill", billController);

            // start with a view
            politicianController.Index();
        }
	}
}

