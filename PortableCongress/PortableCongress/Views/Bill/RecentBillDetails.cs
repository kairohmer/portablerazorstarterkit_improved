#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PortableCongress.Views.Bill
{
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


[System.CodeDom.Compiler.GeneratedCodeAttribute("RazorTemplatePreprocessor", "4.0.1.96")]
public partial class RecentBillDetails : PortableRazor.ViewBase
{

#line hidden

public override void Execute()
{
WriteLiteral("<style>\r\n    iframe {\r\n        min-width: 100%;\r\n        width: 100px;\r\n        *" +
"width: 100%;\r\n    }\r\n</style>\r\n");


#line 10 "RecentBillDetails.cshtml"
Write(Html.Partial("Test"));


#line default
#line hidden
WriteLiteral("\r\n<div");

WriteLiteral(" data-role=\"header\"");

WriteLiteral(" style=\"overflow:hidden;\"");

WriteLiteral(" data-position=\"fixed\"");

WriteLiteral(">\r\n    <h1>Bill Details</h1>\r\n</div>\r\n\r\n<div");

WriteLiteral(" data-role=\"header\"");

WriteLiteral(" style=\"overflow:hidden;\"");

WriteLiteral(" data-position=\"fixed\"");

WriteLiteral(">\r\n");

WriteLiteral("\t");


#line 16 "RecentBillDetails.cshtml"
Write(Html.ActionLink("Back", "ShowRecentBillList", "Bill", new { politicianid = @ViewBag.Model.PoliticianId}, new {
    @class="ui-btn-left ui-btn ui-icon-back ui-btn-icon-notext ui-shadow ui-corner-all",
    data_icon = "arrow-l",
    data_role="button",
    data_mini="true",
    data_inline="true",
    data_transition="slide"}));


#line default
#line hidden
WriteLiteral("\r\n    <h1>Bill Details</h1>\r\n");

WriteLiteral("    ");


#line 24 "RecentBillDetails.cshtml"
Write(Html.ActionLink("Save", "AddFavoriteBill", "Bill", new {id = @ViewBag.Model.Id}, new { 
		@class="ui-btn-right ui-btn ui-btn-icon-notext ui-shadow ui-corner-all",
		data_role="button", 
		data_mini="true", 
		data_inline="true"}));


#line default
#line hidden
WriteLiteral("\r\n</div>\t\r\n\t\t\r\n<div");

WriteLiteral(" style=\"margin-left:10px\"");

WriteLiteral(" data-mini=\"true\"");

WriteLiteral(" data-inset=\"false\"");

WriteLiteral(">\r\n\t<p>");


#line 32 "RecentBillDetails.cshtml"
Write(ViewBag.Model.Title);


#line default
#line hidden
WriteLiteral("</p>\r\n</div>\r\n\r\n<iframe");

WriteLiteral(" scrolling=\"no\"");

WriteAttribute ("src", " src=\"", "\""

#line 35 "RecentBillDetails.cshtml"
, Tuple.Create<string,object,bool> ("", ViewBag.Model.ThomasLink

#line default
#line hidden
, false)
);
WriteLiteral("></iframe>\r\n");

}
}
}
#pragma warning restore 1591
