﻿using SQLite;

namespace PortableCongress
{
    [Table("FavoriteBills")]
    public class Bill
    {
        [PrimaryKey, AutoIncrement]
        [Column("id")]
        public int Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("id")]
        public int PoliticianId { get; set; }

        [Column("thomas_link")]
        public string ThomasLink { get; set; }

        [Column("notes")]
        public string Notes { get; set; }
    }
}