﻿using System;
using System.IO;
using System.Reflection;

namespace Congress
{
	public static class EmeddedResources
	{
	    public static Assembly GetAssembly()
	    {
	        return typeof (EmeddedResources).GetTypeInfo().Assembly;
	    }

	    public static String[] GetEmbeddedContentFolders()
	    {
            return new [] 
            {
                // unfortunately, the identifier of the embedded resources is the original file path
                // while the slashes are replaced by dots. This makes it hard to identify dots inside of file names.
                // by specifying all folders and sub folders, there is no ambiguity...
                "App_Data",
                "www/css",
                "www/images",
                "www/images/icons-png",
                "www/scripts"
            };
	    }
	}
}

