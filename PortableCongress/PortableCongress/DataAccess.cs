using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using PortableCongress;

namespace Congress
{
    public class DataAccess
    {
        public static readonly String DatabaseName = "congress.sqlite";

        private readonly String _ConnectionString;

        public DataAccess(String connectionString)
        {
            _ConnectionString = connectionString;
        }


        public Politician LoadPolitician (int id)
		{
            var db = new SQLiteConnection(_ConnectionString);
            var list = db.Query<Politician>($"SELECT * FROM Politician WHERE govtrack_id = '{id}'");
            return list[0];
		}

		public List<Politician> LoadAllPoliticans ()
		{
            var db = new SQLiteConnection(_ConnectionString);
            var list = db.Query<Politician>("SELECT bioguide_id, first_name, last_name, govtrack_id, phone, party, state FROM Politician ORDER BY last_name");
		    return list;
		}

        public void SaveFavoriteBill (Bill bill)
        {
            var db = new SQLiteConnection(_ConnectionString);
            db.InsertOrReplace(bill);
        }

        public void DeleteFavoriteBill (int id)
        {
            var db = new SQLiteConnection(_ConnectionString);
            db.Delete<Bill>(id);
        }

        public List<Bill> LoadFavoriteBills ()
        {
            var db = new SQLiteConnection(_ConnectionString);
            var list = db.Query<Bill>("SELECT * FROM FavoriteBills");
            return list;
        }

        public Bill LoadFavoriteBill (int id)
        {
            var db = new SQLiteConnection(_ConnectionString);
            var list = db.Query<Bill>($"SELECT * FROM FavoriteBills WHERE id = '{id}'");
            return list[0];
        }

        public void SaveNotes (int id, string notes)
        {
            var db = new SQLiteConnection(_ConnectionString);
            var bills = db.Query<Bill>($"SELECT * FROM FavoriteBills WHERE id = '{id}'");
            if (bills.Count == 1)
            {
                bills[0].Notes = notes;
                db.Update(bills[0]);
            }
        }
	}
}