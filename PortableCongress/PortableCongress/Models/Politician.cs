﻿using System;
using SQLite;

namespace PortableCongress
{
	public class Politician
	{
		public string Name { get; set; }
        public string TwitterId { get; set; }
        public string YoutubeUrl { get; set; }
        public string WebSite { get; set; }

        [Column("bioguide_id")]
        public string BioGuideId { get; set; }

        [Column("first_name")]
		public string FirstName { get; set; }

        [Column("last_name")]
		public string LastName { get; set; }

        [Column("address")]
        public string OfficeAddress { get; set; }

        [Column("govtrack_id")]
        public string GovTrackId { get; set; }

        [Column("phone")]
        public string Phone {get; set; }

        [Column("party")]
        public string Party { get; set; }

        [Column("state")]
        public string State { get; set; }

		int id;
		public int Id {
			get {
				id = int.Parse (GovTrackId);
				return id;
			}
			set{ id = value; }
		}

		string imageName;
		public string ImageName { 
			get{
				imageName = String.Format("https://www.govtrack.us/data/photos/{0}-100px.jpeg", GovTrackId);
				return imageName;
			}
			set{
				imageName = value;
			}
		}
	}
}