using System.IO;
using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;
using AndroidCongress.OSLayer;
using Congress;
using PortableCongress;
using PortableRazor;
using PortableRazor.Andorid;

namespace AndroidCongress
{
	[Activity (Label = "@string/app_name", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class MainActivity : Activity
	{
	    private AndroidPermissions _Permissions;

        protected override async void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // platform web view to display the content
            // there is probably only one which is passed to every controller
            var webView = FindViewById<RazorWebView>(Resource.Id.webView);

            _Permissions = new AndroidPermissions(this);
            _Permissions.AddPermission(Manifest.Permission.Internet);
            await _Permissions.RequestPermissionsAsync();

            // extracts the embedded data
            // this can be done for multiple assemblies
            var resourceManager = new ResourceManager();
            resourceManager.ExtractResources(
                Congress.EmeddedResources.GetAssembly(),
                Congress.EmeddedResources.GetEmbeddedContentFolders());

            // setup database access
            // this shared between the controllers, but this is up to the application
            var dataAccess = new DataAccess(Path.Combine(BaseResourceManager.ContentPath, "App_Data", DataAccess.DatabaseName));

            // application with two controllers
            var politicianController = new PoliticianController(webView, dataAccess);
            var billController = new BillController(webView, dataAccess);

            // setup routing (only one controller here)
            PortableRazor.RouteHandler.RegisterController("Politician", politicianController);
            PortableRazor.RouteHandler.RegisterController("Bill", billController);

            // start with a view
            politicianController.Index();
        }

	    public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
	    {
            _Permissions.ProcessPermissionResult(requestCode, permissions, grantResults);
	        base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
	    }
	}
}


