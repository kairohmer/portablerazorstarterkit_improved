PortableRazorStarterKit_Improved
=======================

This is a fork of a Xamarin Sample Project you can find here:
https://github.com/xamarin/PortableRazorStarterKit

Changes I made just for testing how I works and if I like it:

* added Universal Windows App using the shared portable project, too
* updated the content handling to be a bit more general
* some minor clean-ups and comments