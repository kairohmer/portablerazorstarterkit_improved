using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidCongress.OSLayer
{
    public class AndroidPermissions
    {
        private const int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

        private readonly List<string> _PermissionKeys = new List<string>();
        private readonly Activity _Activity;
        private bool _ShowRational = false;
        private readonly SemaphoreSlim _RequestWaitHandle = new SemaphoreSlim(0, 1);

        public AndroidPermissions(Activity activity)
        {
            _Activity = activity;
        }

        public void AddPermission(string key)
        {
            // new runtime permission checks
            if ((int)(Build.VERSION.SdkInt) < 23)
                return;

            // ignore duplicates
            if (_PermissionKeys.Contains(key))
                return;

            if (_Activity.CheckSelfPermission(key) != Permission.Granted)
            {
                // Check for Rationale Option
                if (_Activity.ShouldShowRequestPermissionRationale((key)))
                {
                    _ShowRational = true;
                }
                _PermissionKeys.Add(key);
            }
        }

        public async Task RequestPermissionsAsync()
        {
            // new runtime permission checks
            if ((int)(Build.VERSION.SdkInt) < 23)
                return;

            // no Permissions needed
            if (_PermissionKeys.Count == 0)
                return;

            // show rational
            if (_ShowRational)
            {
                // todo
            }

            _Activity.RunOnUiThread(() =>
            {
                _Activity.RequestPermissions(_PermissionKeys.ToArray(), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            });
            await _RequestWaitHandle.WaitAsync();
        }


        public void ProcessPermissionResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            switch (requestCode)
            {
                case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
                    for (int i = 0; i < permissions.Length; i++)
                        if (grantResults[i] == Permission.Denied)
                        {
                            // error case
                            // print log:  $"Request Denied: {args.Permissions[i]}
                        }

                    _RequestWaitHandle.Release();
                    return;

                default:
                    return;
            }
        }
    }
}