﻿using System;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Congress;
using PortableCongress;
using PortableRazor;
using PortableRazor.UWP;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWCongress
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            // platform web view to display the content
            // there is probably only one which is passed to every controller
            // webView;

            // extracts the embedded data
            // this can be done for multiple assemblies
            var resourceManager = new ResourceManager();
            resourceManager.ExtractResources(
                Congress.EmeddedResources.GetAssembly(), 
                Congress.EmeddedResources.GetEmbeddedContentFolders());

            // setup database access
            // this shared between the controllers, but this is up to the application
            var dataAccess = new DataAccess(Path.Combine(BaseResourceManager.ContentPath, "App_Data", DataAccess.DatabaseName));          

            // application with two controllers
            var politicianController = new PoliticianController(webView, dataAccess);
            var billController = new BillController(webView, dataAccess);

            // setup routing (only one controller here)
            PortableRazor.RouteHandler.RegisterController("Politician", politicianController);
            PortableRazor.RouteHandler.RegisterController("Bill", billController);

            // start with a view
            politicianController.Index();
        }
    }
}
