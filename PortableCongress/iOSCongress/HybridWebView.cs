using System;
using Foundation;
using UIKit;
using PortableCongress;
using PortableRazor;

namespace iOSCongress
{
	class HybridWebView : IHybridWebView {
		UIWebView webView;

		public HybridWebView(UIWebView uiWebView, String basePath) {
			webView = uiWebView;
		    BasePath = basePath;

			webView.ShouldStartLoad += HandleShouldStartLoad;
		}

		bool HandleShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType) {
			var handled = RouteHandler.HandleRequest (request.Url.AbsoluteString);
			return !handled;
		}

		#region IHybridWebView implementation

        public String BasePath { get; private set; }

		public void LoadHtmlString (string html)
		{
			var url = new NSUrl (BasePath, true);
			webView.LoadHtmlString (html, url);
		}

		public string EvaluateJavascript (string script) 
		{
			return webView.EvaluateJavascript (script);
		}

		#endregion
	}
}

